from django.contrib import admin

from .models import *


admin.site.register(Embranchement)
admin.site.register(Classe)
admin.site.register(Ordre)
admin.site.register(Famille)
admin.site.register(Genre)
admin.site.register(Espece)
admin.site.register(Terrestre)
admin.site.register(Aquatique)
admin.site.register(Animal)


#class AnimalAdmin(admin.ModelAdmin):
#    list_display = ('nom_commun', 'nom_scientifique')
#    search_fields = ['nom_commun']



