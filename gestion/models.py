from django.db import models


class Embranchement(models.Model):
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Classe(models.Model):
    embranchement = models.ForeignKey(Embranchement, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Ordre(models.Model):
    classe = models.ForeignKey(Classe, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Famille(models.Model):
    ordre = models.ForeignKey(Ordre, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Genre(models.Model):
    famille = models.ForeignKey(Famille, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Espece(models.Model):
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class Animal(models.Model):
    espece = models.ForeignKey(Espece, on_delete=models.CASCADE)
    nom_commun = models.CharField(max_length=50)

    @property
    def nom_scientifique(self):
        return "{0} {1}".format(self.espece.genre, self.espece)

    def __str__(self):
        return self.nom_commun

    class Meta:
        verbose_name_plural = "animaux"


class Terrestre(Animal):
    terrestre_to_animal = models.OneToOneField(Animal, parent_link=True, on_delete=models.CASCADE)
    hygro_min = models.IntegerField("hygrométrie minimum", default=0)
    hygro_max = models.IntegerField("hygrométrie maximum", default=0)


class Aquatique(Animal):
    aquatique_to_animal = models.OneToOneField(Animal, parent_link=True, on_delete=models.CASCADE)
    temp_eau_min = models.IntegerField("température de l'eau minimum", default=0)
    temp_eau_max = models.IntegerField("température de l'eau maximum", default=0)
    durete_min = models.IntegerField("dureté minimum", default=0)
    durete_max = models.IntegerField("dureté maximum", default=0)
    ph_min = models.IntegerField("pH minimum", default=0)
    ph_max = models.IntegerField("pH maximum", default=0)
    densite_min = models.IntegerField("densité minimum", default=0)
    densite_max = models.IntegerField("densité maximum", default=0)
